﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepsScript : MonoBehaviour
{
    public AudioClip[] _clip;
    AudioSource _source;
    int SampleIndex;


    // Start is called before the first frame update
    void Start()
    {
        _source = gameObject.AddComponent<AudioSource>();

    }

    void footstep()
    {
        SampleIndex = Random.Range(0, 7);

        _source.clip = _clip[SampleIndex];
        _source.volume = Random.Range(0.8f, 1f);
        _source.pitch = Random.Range(0.8f, 1.1f);
        _source.Play();






    }
}